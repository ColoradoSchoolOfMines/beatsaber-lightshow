# Light Groups
BACK_LASER = 'back_laser'
RING_NEONS = 'ring_neons'
LEFT_LASER = 'left_laser'
RIGHT_LASER = 'right_laser'
CENTER_LIGHT = 'center_light'

# Light States
LIGHT_OFF = 'off'

BLUE_ON = 'blue_on'
BLUE_FLASH = 'blue_flash'
BLUE_FADE = 'blue_fade'

RED_ON = 'red_on'
RED_FLASH = 'red_flash'
RED_FADE = 'red_fade'

# Colors
LIGHT_BLUE = (21, 244, 255)
BLUE = (0, 0, 255)
LIGHT_RED = (255, 68, 119)
RED = (255, 0, 0)
BLACK = (0, 0, 0)

def is_raspberry_pi():
    try:
        # Read kernel device tree
        with open('/sys/firmware/devicetree/base/model', 'r') as model:
            if 'raspberry pi' in model.read().lower():
                return True
    except:
        pass
    return False

class Color():
    def __init__(self, color):
        self.color = color

    def get_color(self, delta_time):
        return self.color


class InterpolatatingColor():
    class Curves():
        linear = lambda time, duration: time/duration
        quadratic = lambda time, duration: (time/duration)**2

    def __init__(self, start_color, end_color, duration, curve):
        self.start_color = start_color
        self.end_color = end_color
        self.duration = duration
        self.curve = curve
        self.elapsed_time = 0.0

    def get_color(self, delta_time):
        self.elapsed_time += delta_time
        position = self.curve(self.elapsed_time, self.duration)

        r = self._interpolate(self.start_color[0], self.end_color[0], position)
        g = self._interpolate(self.start_color[1], self.end_color[1], position)
        b = self._interpolate(self.start_color[2], self.end_color[2], position)

        return (r, g, b)

    def _interpolate(self, val1, val2, position):
        if position > 1.0:
            position = 1.0
        elif position < 0.0:
            position = 0.0

        return val1 + (val2 - val1) * float(position)

class ConsoleLightController():
    def set_state(self, light_group, light_state):
        print("%s --> %s" % (light_group, light_state))

    def update(self, _):
        pass


class LedLightController():
    # These imports can only resolve on NeoPixel-capable device
    try:
        import board
        import neopixel

        NEOPIXEL_PIN = board.D18
        NUM_LEDS = 300
        MAX_BRIGHTNESS = 1
        FADE_TIME_MS = 250
    except ImportError:
        pass
    except NotImplementedError:
        pass

    class NeoPixelGroup():
        """
        group_members is a list or range of LEDs that this NeoPixelGroup can control
        """
        def __init__(self, group_members, pixels):
            self.group_members = group_members
            self.pixels = pixels

            self.active_pixel_colors = [None]*(len(pixels))

        def set_state(self, state):
            if state is BLUE_ON or state is RED_ON:
                for member in self.group_members:
                    self.active_pixel_colors[member] = Color(BLUE if state is BLUE_ON else RED)
            elif state is BLUE_FADE or state is RED_FADE:
                for member in self.group_members:
                    self.active_pixel_colors[member] = InterpolatatingColor(
                        BLUE if state is BLUE_FADE else RED,
                        BLACK,
                        700,
                        InterpolatatingColor.Curves.linear
                    )
            elif state is BLUE_FLASH or state is RED_FLASH:
                for member in self.group_members:
                    self.active_pixel_colors[member] = InterpolatatingColor(
                        LIGHT_BLUE if state is BLUE_FLASH else LIGHT_RED,
                        BLUE if BLUE_FLASH else RED,
                        600,
                        InterpolatatingColor.Curves.quadratic
                    )
            elif state is LIGHT_OFF:
                for member in self.group_members:
                    self.active_pixel_colors[member] = Color(BLACK)

        def update(self, deltaTime):
            for member in self.group_members:
                if self.active_pixel_colors[member] != None:
                    self.pixels[member] = self.active_pixel_colors[member].get_color(deltaTime)

    # GPIO = General Purpose Input/Output on the Raspberry Pi
    # Used to control individual LEDs/peripherals outside the LED
    # strip. Could use this if we want more lights
    class GpioGroup():
        def __init__(self, group_members, z_index):
            self.group_members = group_members
        
        def set_state(self, state):
            pass

    def __init__(self):
        self.pixels = self.neopixel.NeoPixel(
            self.NEOPIXEL_PIN,
            self.NUM_LEDS,
            brightness=self.MAX_BRIGHTNESS,
            auto_write=False
        )

        self.LIGHT_GROUPS = {
            LEFT_LASER: self.NeoPixelGroup(list(range(0, 90)), self.pixels),
            BACK_LASER: self.NeoPixelGroup(list(range(90, 100))+list(range(200, 210)), self.pixels),
            CENTER_LIGHT: self.NeoPixelGroup(list(range(100, 125))+list(range(175, 200)), self.pixels),
            RING_NEONS: self.NeoPixelGroup(list(range(125, 175)), self.pixels),
            RIGHT_LASER: self.NeoPixelGroup(list(range(210, 300)), self.pixels)
        }

    def set_state(self, light_group, light_state):
        if light_group in self.LIGHT_GROUPS.keys():
            self.LIGHT_GROUPS[light_group].set_state(light_state)

    def update(self, delta_time):
        for group in self.LIGHT_GROUPS.keys():
            self.LIGHT_GROUPS[group].update(delta_time)
        
        self.pixels.show()


def get_controller():
    if is_raspberry_pi():
        print("Raspberry Pi Detected: Enabling LED control mode")
        return LedLightController()
    print("This device cannot control LEDs, switching to Console Light Mode")
    return ConsoleLightController()
