# beatsaber-lightshow

A python script to turn a Beat Saber custom map into an interactive light show. The physical hardware
controlled is to be LED light strips driven by Raspberry Pi GPIO pins.

## Requirements
Please use Python 3. Also use pipenv. The `pipfile.lock` should force Python 3.8. For now, this
project will use pygame to play sound and keep track of time. Also handy
for creating a UI should we be so bold as to do so.

## JSON Format
```
{
	"_version": "2.0.0",
	"_events": [{
		"_time": 4.927083, // What BEAT to execute on
		"_type": 2, // Left laser
		"_value": 1 // Blue on
	}, {
		"_time": 5.927083,
		"_type": 2, // Left laser
		"_value": 0 // Blue off
	}, {
		"_time": 6.927083,
		"_type": 3, // Right laser
		"_value": 1 // Blue on
	}, {
		"_time": 7.927083,
		"_type": 3, // Right laser
		"_value": 0 // Blue off
	}, {
		"_time": 8.927083,
		"_type": 2, // Left laser
		"_value": 5 // Red on
	}, {
		"_time": 9.927083,
		"_type": 2, // Left laser
		"_value": 0 // Red off
	}, {
		"_time": 10.927083,
		"_type": 3, // Right laser
		"_value": 5 // Red on
	}, {
		"_time": 11.927083,
		"_type": 3, // Right laser
		"_value": 0 // Red off
	}, {
		"_time": 12.927083,
		"_type": 2, // Left laser
		"_value": 3 // Blue fade
	}, {
		"_time": 13.927083,
		"_type": 3, // Right laser
		"_value": 7 // Red fade
	}, {
		"_time": 14.927083,
		"_type": 0, // Back laser
		"_value": 2 // Blue flash and hold
	}, {
		"_time": 15.927083,
		"_type": 0, // Back laser
		"_value": 0 // Blue off
	}, {
		"_time":16.927083,
		"_type": 4, // Primary (track) light
		"_value": 6 // Red flash and hold
	}, {
		"_time": 17.927083,
		"_type": 4, // Primary (track) light
		"_value": 0 // Red off
	}, {
		"_time": 18.927083,
		"_type": 1, // Ring neons
		"_value": 1 // Blue on
	}, {
		"_time": 19.927083,
		"_type": 1, // Ring neons
		"_value": 0 // Blue off
	}],
	"_notes": [], // Unused for our purposes
	"_obstacles": [], // Also unused
	"_customData": { // Potentially used by Chroma; not sure
		"_bookmarks": []
	}
}
```
So, to summarize:  

`_time` determines which beat the event occurs on. Because this is BPM dependent,
we also need to pull in BPM from the info.dat file.

`_type` determines which light to turn on. Possible values are as follows:
* 0: Back laser
* 1: Ring neons
* 2: Left laser
* 3: Right laser
* 4: Primary light (lights on either side of the track which notes travel along in the game)

`_value` determines what to do to the light. Possible values are as follows:
* 0: Turn off light
* 1: Turn blue light on
* 2: Flash blue and keep light on
* 3: Flash blue and fade light off
* 4: Unused (although probably for turning off red lights)
* 5: Turn red light on
* 6: Flash red and keep light on
* 7: Flash red and fade light off
