import json
import os

def get_valid_folders(search_dir):
    valid_dirs = []
    for file in os.listdir(search_dir):
        d = os.path.join(search_dir, file)
        if os.path.isdir(d) and (os.path.isfile(d + '/info.dat') or os.path.isfile(d + '/Info.dat')):
            valid_dirs.append(d)
            print('Valid dir found', d)
    return valid_dirs

def parse_info_file(file_path):
    fin = open(file_path, 'r')
    fin_data = fin.read().replace('\n','')
    json_data = json.loads(fin_data)
    fin.close()
    return {
        'name': json_data['_songName'],
        'artist': json_data['_songAuthorName'],
        'bpm': json_data['_beatsPerMinute'],
        'song': json_data['_songFilename'],
        'maps': json_data['_difficultyBeatmapSets'][0]['_difficultyBeatmaps'],
    }

def parse_beatmap_file(file_path):
    try:
        fin = open(file_path, 'r')
    except FileNotFoundError:
        print('Beatmap file not found! Try another song.')
        return False
    fin_data = fin.read().replace('\n','')
    json_data = json.loads(fin_data)
    fin.close()
    return json_data['_events']